// #define F_CPU 1000000UL
#include <avr/io.h>
#include <util/delay.h>

volatile PORT_t & PORT = PORTA;
constexpr uint8_t PIN = PIN5;

constexpr double DELAY_TIME_MS = 500;
int main(void)
{
	//disable region protection for 4 clock cycles; allow to change clock settings
	CCP = CCP_IOREG_gc;
	// change clock source from internal 20MHz to internal 32.768kHz
	CLKCTRL.MCLKCTRLA = CLKCTRL_CLKSEL_OSCULP32K_gc;
	
	//disable region protection for 4 clock cycles; allow to change clock settings
	CCP = CCP_IOREG_gc;
	// enable prescaler, /32 division
	CLKCTRL.MCLKCTRLB = CLKCTRL_PEN_bm | CLKCTRL_PDIV_32X_gc;
	// Result: power draw went from 1.5mA down to 230uA

	// set output pin mode
	// PORT.DIR |= (1<<PIN);
	PORT.DIRSET = (1<<PIN);
	
	// enable pullup
	// PORT.OUT |= (1<<PIN);
	PORT.OUTSET = (1<<PIN);
	

	while (1) {
		_delay_ms(DELAY_TIME_MS);

		// toggle output
		// PORT.OUT ^= (1<<PIN);
		PORT.OUTTGL = (1<<PIN);

	}
}
