Import("env")

# install pymcuprog, the UART UPDI uploader
# It's role is similar to AVRDUDE
try:
    import pymcuprog
except ImportError:
    env.Execute("$PYTHONEXE -m pip install pymcuprog")